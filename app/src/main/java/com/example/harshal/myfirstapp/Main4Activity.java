package com.example.harshal.myfirstapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Main4Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        Button bt1 = (Button)findViewById(R.id.p1);
        Button bt2 = (Button)findViewById(R.id.p2);
        Button bt3 = (Button)findViewById(R.id.p3);
        Button bt4 = (Button)findViewById(R.id.p4);
        Button bt5 = (Button)findViewById(R.id.p5);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main4Activity.this, Printer1.class);
                startActivity(int1);
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main4Activity.this, Printer2.class);
                startActivity(int1);
            }
        });
        bt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main4Activity.this, Printer3.class);
                startActivity(int1);
            }
        });
        bt4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main4Activity.this, Printer4.class);
                startActivity(int1);
            }
        });
        bt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main4Activity.this, Printer5.class);
                startActivity(int1);
            }
        });


    }

}
