package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Printer1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer1);
        ViewPager viewPager1 = findViewById(R.id.viewPagerp1);
        PA1 adapter =  new PA1(this);
        viewPager1.setAdapter(adapter);
    }
}
