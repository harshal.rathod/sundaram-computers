package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class laptop5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laptop5);
        ViewPager viewPager1 = findViewById(R.id.viewPager5);
        IA5 adapter =  new IA5(this);
        viewPager1.setAdapter(adapter);
    }
}
