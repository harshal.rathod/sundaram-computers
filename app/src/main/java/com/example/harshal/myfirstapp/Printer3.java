package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Printer3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer3);
        ViewPager viewPager1 = findViewById(R.id.viewPagerp3);
        PA3 adapter =  new PA3(this);
        viewPager1.setAdapter(adapter);
    }
}
