package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Printer5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer5);
        ViewPager viewPager1 = findViewById(R.id.viewPagerp5);
        PA5 adapter =  new PA5(this);
        viewPager1.setAdapter(adapter);
    }
}
