package com.example.harshal.myfirstapp;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import static com.example.harshal.myfirstapp.R.id.viewPager1;

public class Laptop2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laptop2);
        ViewPager viewPager1 = findViewById(R.id.viewPager1);
        IA2 adapter =  new IA2(this);
        viewPager1.setAdapter(adapter);
    }

}
