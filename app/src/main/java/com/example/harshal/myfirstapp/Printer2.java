package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Printer2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer2);
        ViewPager viewPager1 = findViewById(R.id.viewPagerp2);
        PA2 adapter =  new PA2(this);
        viewPager1.setAdapter(adapter);
    }
}
