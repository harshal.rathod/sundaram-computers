package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Laptop8 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laptop8);
        ViewPager viewPager1 = findViewById(R.id.viewPager8);
        IA8 adapter =  new IA8(this);
        viewPager1.setAdapter(adapter);
    }
}
