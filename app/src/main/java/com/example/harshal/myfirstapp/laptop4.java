package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class laptop4 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laptop4);
        ViewPager viewPager1 = findViewById(R.id.viewPager4);
        IA4 adapter =  new IA4(this);
        viewPager1.setAdapter(adapter);
    }
}
