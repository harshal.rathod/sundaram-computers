package com.example.harshal.myfirstapp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Laptop1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laptop1);

        ViewPager viewPager = findViewById(R.id.viewPager);
        ImageAdapter adapter =  new ImageAdapter(this);
        viewPager.setAdapter(adapter);
       }

}
