package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Laptop3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laptop3);
        ViewPager viewPager1 = findViewById(R.id.viewPager3);
        IA3 adapter =  new IA3(this);
        viewPager1.setAdapter(adapter);
    }
}
