package com.example.harshal.myfirstapp;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Harshal on 16-04-2018.
 */

public class IA7 extends PagerAdapter {
    private Context mContext;
    private int[] mImageIds = new int[] {R.drawable.lp_7, R.drawable.lp_71, R.drawable.lp_72,R.drawable.lp_73,R.drawable.lp_74,R.drawable.lp_75};
    IA7(Context context) {
        mContext = context;
    }
    @Override
    public int getCount() {
        return mImageIds.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView= new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageResource(mImageIds[position]);
        container.addView(imageView,0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}
