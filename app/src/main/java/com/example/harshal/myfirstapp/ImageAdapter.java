package com.example.harshal.myfirstapp;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Harshal on 31-03-2018.
 */

public class ImageAdapter extends PagerAdapter {
    private Context mContext;
    private  int [] mImageIds = new int[] {R.drawable.lp1, R.drawable.lp2, R.drawable.lp3, R.drawable.lp4, R.drawable.lp5, R.drawable.lp11};

    ImageAdapter(Context context) {
        mContext=context;
    }
    @Override
    public int getCount() {
        return mImageIds.length ;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView= new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageResource(mImageIds[position]);
        container.addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}

