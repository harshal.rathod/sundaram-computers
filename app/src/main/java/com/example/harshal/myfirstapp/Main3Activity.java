package com.example.harshal.myfirstapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Button btn1 = (Button)findViewById(R.id.lp1);
        Button btn2 = (Button)findViewById(R.id.lp2);
        Button btn3 = (Button)findViewById(R.id.lp3);
        Button btn4 = (Button)findViewById(R.id.lp4);
        Button btn5 = (Button)findViewById(R.id.lp5);
        Button btn6 = (Button)findViewById(R.id.lp6);
        Button btn7 = (Button)findViewById(R.id.lp7);
        Button btn8 = (Button)findViewById(R.id.lp8);



        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main3Activity.this, Laptop1.class);
                startActivity(int1);
            }
        });
    btn2.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent int2=new Intent( Main3Activity.this ,Laptop2.class );
            startActivity(int2);
        }
    });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main3Activity.this, Laptop3.class);
                startActivity(int1);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main3Activity.this, laptop4.class);
                startActivity(int1);
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main3Activity.this, laptop5.class);
                startActivity(int1);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main3Activity.this, Laptop6.class);
                startActivity(int1);
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main3Activity.this, Laptop7.class);
                startActivity(int1);
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent int1=new Intent(Main3Activity.this, Laptop8.class);
                startActivity(int1);
            }
        });


    }
    }
