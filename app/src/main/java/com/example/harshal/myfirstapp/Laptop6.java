package com.example.harshal.myfirstapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Laptop6 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laptop6);
        ViewPager viewPager1 = findViewById(R.id.viewPager6);
        IA6 adapter =  new IA6(this);
        viewPager1.setAdapter(adapter);
    }
}
